=============================
Sale Line quick_actions Scenario
=============================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install sale_line_quick_actions::

    >>> config = activate_modules('sale_line_quick_actions')


Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

    >>> Journal = Model.get('account.journal')
    >>> PaymentMethod = Model.get('account.invoice.payment.method')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.save()
    >>> payment_method = PaymentMethod()
    >>> payment_method.name = 'Cash'
    >>> payment_method.journal = cash_journal
    >>> payment_method.credit_account = cash
    >>> payment_method.debit_account = cash
    >>> payment_method.save()

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product, = template.products

    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.account_category = account_category
    >>> template.save()
    >>> service, = template.products

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Sale 5 products::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 2.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 3.0
    >>> sale.save()
    >>> sale_line = sale.lines[0]

Check duplicate method::

    >>> Modeldata = Model.get('ir.model.data')
    >>> data, = Modeldata.find([
    ...     ('module', '=', 'sale_line_quick_actions'),
    ...     ('fs_id', '=', 'wizard_quick_actions_copy')])
    >>> duplicate_line = Wizard('sale.line.quick_action', [sale_line], action={'id': data.db_id})
    >>> duplicate_line.form.copies
    1
    >>> duplicate_line.execute('do_duplicate')
    >>> sale.reload()
    >>> len(sale.lines)
    3
    >>> line1, line2, line3 = sale.lines
    >>> line1.quantity == line3.quantity
    True
    >>> line1.product == line3.product
    True

Check split method::

    >>> data, = Modeldata.find([
    ...     ('module', '=', 'sale_line_quick_actions'),
    ...     ('fs_id', '=', 'wizard_quick_actions_split')])
    >>> split_line = Wizard('sale.line.quick_action', [sale_line], action={'id': data.db_id})
    >>> split_line.form.quantity
    2.0
    >>> split_line.form.quantity = 1.0
    >>> split_line.execute('do_split')
    >>> sale.reload()
    >>> len(sale.lines)
    4
    >>> line1, _, _, line4 = sale.lines
    >>> line1.quantity == line4.quantity == 1.0
    True

Check move method::

    >>> sale2 = Sale()
    >>> sale2.party = customer
    >>> sale2.payment_term = payment_term
    >>> sale2.save()
    >>> data, = Modeldata.find([
    ...     ('module', '=', 'sale_line_quick_actions'),
    ...     ('fs_id', '=', 'wizard_quick_actions_move')])
    >>> move_line = Wizard('sale.line.quick_action', [sale_line], action={'id': data.db_id})
    >>> move_line.form.sale = sale2
    >>> move_line.execute('do_move')
    >>> sale.reload()
    >>> len(sale.lines)
    3
    >>> sale2.reload()
    >>> len(sale2.lines)
    1
